# Desafio Automacao teste

Documentação do projeto:
O projeto consiste em consultar um CPF que possui ou não alguma restrição. 
Dada consulta, caso este CPF não haja restrição, é realizada uma simualçao de crédito para o mesmo.
Nesta simulação, é descrita o crédito de valor, a quantidade de parcelas, dados de contato, etc.


Para a realização deste projeto, foi utilizado para a configuração de ambiente:
- Java 8+ JDK
- Apache Maven para o gerenciamento de dependencias e build;

Bibliotecas utilizadas:
- lombok: utilizado para gerar os metodos get e set e para o build dos objetos.
- rest-assured: para a chamada dos serviços(given)
- TestNG: para execução dos métodos de teste.


Visualizar relatório de testes:
target/surefire-reports/api.automacao.validaRestricoes/

Instrução para execução dos testes via terminal:
 
 script:
    - mvn test -Dtest=validaRestricoes
	
	script:
    - mvn test -Dtest=validaSimulacoes




