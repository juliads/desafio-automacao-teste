package api;

import org.testng.annotations.BeforeClass;
import static io.restassured.RestAssured.*;

import static io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails;

public abstract class UrlBase {
    @BeforeClass
    public void preCondition () {
        baseURI = "http://localhost:8080/";
        basePath = "/api/v1/";
        enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
