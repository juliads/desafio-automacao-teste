package api.dataProvider;

import api.dto.RestricaoDTO;
import org.testng.annotations.DataProvider;

public class GeradorRestricoes {

    @DataProvider(name = "gerarComRestricao")
    public static Object[] gerarComRestricao() {
        return new Object[]{
                RestricaoDTO
                        .builder()
                        .cpf("62648716050")
                        .build()
        };
    }

    @DataProvider(name = "gerarSemRestricao")
    public static Object[] gerarSemRestricao() {
        return new Object[]{
                RestricaoDTO
                        .builder()
                        .cpf("03738080031")
                        .build()
        };
    }
}