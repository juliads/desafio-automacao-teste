package api.dataProvider;

import api.dto.SimulacaoDTO;
import org.testng.annotations.DataProvider;

public class GeradorSimulacoes {

    @DataProvider(name="gerarSimulacao")
    public static Object[] gerarSimulacao(){
        return new Object[]{
                SimulacaoDTO
                        .builder()
                        .cpf("01234567890")
                        .nome("Julia Dorneles")
                        .email("julia@teste.com")
                        .valor(1000)
                        .parcelas(2)
                        .seguro(true)
                        .build()
        };
    }
}