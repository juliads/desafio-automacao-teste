package api.automacao;

import api.UrlBase;
import api.dataProvider.GeradorRestricoes;
import api.dataProvider.GeradorSimulacoes;
import api.dto.RestricaoDTO;
import io.restassured.http.ContentType;
import org.hamcrest.CoreMatchers;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class validaRestricoes extends UrlBase {

    //TODO BUG - SISTEMA ESTÁ APRESENTANDO MENSAGEM DIFERENTE DO ESPERADO
    @Test (dataProvider = "gerarComRestricao", dataProviderClass = GeradorRestricoes.class)
    public void consultaCPFRestricao(RestricaoDTO restricaoDTO) {
        String mensagem = "O CPF 62648716050 possui restrição";
        given().
                pathParam("cpf",restricaoDTO.getCpf())
                .contentType(ContentType.JSON)
                .when()
                .get("/restricoes/{cpf}")
                .then()
                .log()
                .all()
                .statusCode(200)
                .body("mensagem", CoreMatchers.equalTo(mensagem));
    }

    @Test (dataProvider = "gerarSemRestricao", dataProviderClass = GeradorRestricoes.class)
    public void consultaCPFSemRestricao(RestricaoDTO restricaoDTO){
        given()
                .pathParam("cpf", restricaoDTO.getCpf())
                .contentType(ContentType.JSON)
                .when()
                .get("/restricoes/{cpf}")
                .then()
                .log()
                .all()
                .statusCode(204);
    }
}
