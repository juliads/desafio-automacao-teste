package api.automacao;

import api.UrlBase;
import api.dataProvider.GeradorSimulacoes;
import api.dto.SimulacaoDTO;
import io.restassured.http.ContentType;
import org.hamcrest.CoreMatchers;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class validaSimulacoes extends UrlBase {
    private int id;

    @Test(priority=1, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void geraSimulacaoComSeguro(SimulacaoDTO simulacaoDTO){

        given()
                .body(simulacaoDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes")
                .then()
                .log().all()
                .statusCode(201);
    }

    @Test(priority=2, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void geraSimulacaoSemSeguro(SimulacaoDTO simulacaoDTO){
        simulacaoDTO.setCpf("69181374097");
        simulacaoDTO.setSeguro(false);

        given()
                .body(simulacaoDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes")
                .then()
                .log().all()
                .statusCode(201);
    }

    //TODO BUG - SISTEMA APRESENTA MENSAGEM DIFERENTE DO ESPERADO
    @Test(priority=3, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void geraSimulacaoComCPFExistente(SimulacaoDTO simulacaoDTO){

        given()
                .body(simulacaoDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes")
                .then()
                .log().all()
                .statusCode(409)
                .body("mensagem", CoreMatchers.equalTo("CPF já existente"));
    }

    @Test(priority=4, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void geraSimulacaoAcimaDoValor(SimulacaoDTO simulacaoDTO){
        simulacaoDTO.setValor(500000);

        given()
                .body(simulacaoDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes")
                .then()
                .log().all()
                .statusCode(400)
                .body("erros.valor", CoreMatchers.equalTo("Valor deve ser menor ou igual a R$ 40.000"));
    }

    //TODO BUG - SISTEMA NÃO FAZ VALIDAÇÃO DE VALOR MINIMO
    @Test(priority=5, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void geraSimulacaoAbaixoDoValor(SimulacaoDTO simulacaoDTO){
        simulacaoDTO.setValor(10);

        given()
                .body(simulacaoDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes")
                .then()
                .log().all()
                .statusCode(400)
                .body("erros.valor", CoreMatchers.equalTo("Valor deve ser maior ou igual a R$ 1.000"));
    }

    @Test(priority=6, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void alteraSimulacaoComSeguro(SimulacaoDTO simulacaoDTO){
        SimulacaoDTO retornoSimulacao = retornaSimulacao(simulacaoDTO.getCpf());
        retornoSimulacao.setNome("Teste Alteracao");

        given()
                .pathParam("cpf", retornoSimulacao.getCpf())
                .body(retornoSimulacao)
                .contentType(ContentType.JSON)
                .when()
                .put("/simulacoes/{cpf}")
                .then()
                .log().all()
                .statusCode(200);
    }

    @Test(priority=7, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void consultaSimulacaoComSeguro(SimulacaoDTO simulacaoDTO){
        given()
                .pathParam("cpf", simulacaoDTO.getCpf())
                .contentType(ContentType.JSON)
                .when()
                .get("/simulacoes/{cpf}")
                .then()
                .log().all()
                .statusCode(200)
                .body("cpf", CoreMatchers.equalTo(""+simulacaoDTO.getCpf()+""));
    }

    @Test(priority=8, dataProvider = "gerarSimulacao", dataProviderClass = GeradorSimulacoes.class)
    public void deletaSimulacao (SimulacaoDTO simulacaoDTO){

        SimulacaoDTO retornoSimulacao = retornaSimulacao(simulacaoDTO.getCpf());

        given()
                .pathParam("id", retornoSimulacao.getId())
                .contentType(ContentType.JSON)
                .when()
                .delete("/simulacoes/{id}")
                .then()
                .log().all()
                .statusCode(200);
    }

    public SimulacaoDTO retornaSimulacao(String cpf){
        return given()
                .pathParam("cpf", cpf)
                .contentType(ContentType.JSON)
                .when()
                .get("/simulacoes/{cpf}")
                .then()
                .log().all()
                .statusCode(200)
                .extract().as(SimulacaoDTO.class);
    }
}